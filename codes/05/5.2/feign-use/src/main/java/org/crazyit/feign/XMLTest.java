package org.crazyit.feign;

import org.crazyit.feign.PersonClient.Person;
import org.crazyit.feign.PersonClient.Result;

import feign.Feign;
import feign.jaxb.JAXBContextFactory;
import feign.jaxb.JAXBDecoder;
import feign.jaxb.JAXBEncoder;

public class XMLTest {

	public static void main(String[] args) {
		JAXBContextFactory jaxbFactory = new JAXBContextFactory.Builder().build();
		// 获取服务接口
		PersonClient personClient = Feign.builder()
				.encoder(new JAXBEncoder(jaxbFactory))
				.decoder(new JAXBDecoder(jaxbFactory))
				.target(PersonClient.class, "http://localhost:8080/");
		// 构建参数
		Person person = new Person();
		person.id = 1;
		person.name = "Angus";
		person.age = 30;
		// 调用接口并返回结果
		Result result = personClient.createPersonXML(person);
		System.out.println(result.message);
	}
}
