package com.netflix.hystrix.crazyit.run;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

/**
 * 测试异步命令
 * 
 * @author 杨恩雄
 *
 */
public class AsyncCommand extends HystrixCommand<String> {


	
	public AsyncCommand() {
		// 设置执行超时时间为2秒
	    super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"))
	            .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
	                   .withExecutionTimeoutInMilliseconds(2000)));
	}

	@Override
	protected String run() throws Exception {
		Thread.sleep(1000);
		System.out.println("这是异步命令，这里会异步执行");
		return "success";
	}

}
