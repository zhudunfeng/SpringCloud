package org.crazyit.feign;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface PersonClient {

	@RequestLine("POST /person/create")
	@Headers("Content-Type: application/json")
	String createPerson(Person person);

	@RequestLine("POST /person/createXML")
	@Headers("Content-Type: application/xml")
	Result createPersonXML(Person person);

	@RequestLine("GET /person/{personId}")
	Person findById(@Param("personId") Integer personId);

	@RequestLine("GET /hello")
	String sayHello();
	

	@Data
	@XmlRootElement
	class Person {
		@XmlElement
		Integer id;
		@XmlElement
		String name;
		@XmlElement
		Integer age;
		@XmlElement
		String message;
	}

	@Data
	@XmlRootElement
	class Result {
		@XmlElement
		String message;
	}
}
