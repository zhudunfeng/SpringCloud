package org.crazyit.feign.contract;

/**
 * 服务客户端
 * @author 杨恩雄
 *
 */
public interface HelloClient {
	
	@MyUrl(method = "GET", url = "/hello")
	String myHello();
}
